import platform
from .plugin import *

os_name = platform.system()

app_platform_name=""
if os_name == "Darwin":
	app_platform_name = "MACOS"
elif os_name == "Windows":
	app_platform_name = "WINDOWS"
elif os_name == "Linux":
	app_platform_name = "LINUX"
else:
	app_platform_name = "UNKNOWN"

print(app_platform_name)

register_data = {
						"name": "SUBLIME",
						"version": "1",
						"platform": app_platform_name
				}
register_data = json.dumps(register_data) # Dump user selected data as a json string