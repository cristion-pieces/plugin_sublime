import json
import sublime
import urllib.parse
import urllib.request
import sublime_plugin
import os
from .create_asset import *
from .status_popups import *
from .register_data import *
from .application import *

_instantiate = Application() # Instantiate a class

# *****************      Create an asset or "piece" from user selected text

class create_assetCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		sel = self.view.sel()[0] # User Generated Data Pt. 1
		selected = self.view.substr(sel) # User Generated Data Pt. 2
		self.view.show_popup(html_fail, max_width=512) # Generate Failure Message
		createAsset(selected) # Create An Asset with User Selected Text
		self.view.show_popup(html_success, max_width=512) # Generate Success Message to Override Failure if asset created successfully

# *****************      *****************      *****************

