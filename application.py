import json
import sublime
import urllib.parse
import urllib.request
import sublime_plugin
import os
from .create_asset import *
from .status_popups import *
from .register_data import *
from .application import *

# Singleton Implementation - Check if instance eqauls none, if so, set instance to self and perform a register event. 
# If instance does not eqaul none, return a message saying this is a singleton implementation. 

class Application:
   # current here is the registered application
   instance = None
   current = None
   @staticmethod 
   def getInstance():
      """ Static access method. """
      if Application.instance == None:
         try:
            Application.instance = self
         except:
            print("Failure to get Application Instance")
      return Application.instance     
   def __init__(self):                          # 1. This function runs when Application() is called 
      """ Virtual private constructor. """
      if Application.instance != None:
         raise Exception("This class is a singleton!")
      else:
         Application.instance = self            # 2. Instance is now set to self
         Application.register()
   def register():
      reqe = urllib.request.Request(url = 'http://localhost:1000/applications/register', data = bytes(register_data.encode("utf-8")), method = "POST")
      with urllib.request.urlopen(reqe) as resp:
         responsed_data = json.loads(resp.read().decode("utf-8"))
         Application.current=responsed_data                           # 3. Register Application

