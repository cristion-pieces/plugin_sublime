# Pieces Plugin 

The purpose of this plugin is to enable a user to seamlessly save, find and reuse code snippets straight from Sublime Text Editor. Visit pieces.app for more information.

## Sublime Text

Developed using Python 3.8.8 and Sublime Text Editor.

## Installation

Download the zip file, and extract it into your Sublime Text > Packages  Directory.

## Usage 

Please ensure Pieces OS and Pieces (see pieces.app) is running on your local machine. For more information about Pieces please visit pieces.app. 

Right-click (Mouse click for MacOS) inside Sublime Text to access context menu and select "Send to Pieces".



Mesh Intelligent Communications © 2021. All Rights Reserved


