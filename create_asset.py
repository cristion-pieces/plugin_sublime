from .plugin import *
from .register_data import *
from .application import *
# Version 1 : Register Application + Create an Asset

def createAsset(selected):
	asset_data = {
							"asset": {
								"application": Application.current,
								"format": {
									"fragment":{
										"value": selected
									}
								}
							},
							"type": "SEEDED_FORMAT"
		}
	asset_data = json.dumps(asset_data) # Dump user selected data as a json string
	req = urllib.request.Request(url = 'http://localhost:1000/assets/create', data = bytes(asset_data.encode("utf-8")), method = "POST")
	with urllib.request.urlopen(req) as resp:
		response_data = json.loads(resp.read().decode("utf-8"))
		print(response_data)